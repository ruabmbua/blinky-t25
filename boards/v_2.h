#ifndef BOARDS_V_2_H__
#define BOARDS_V_2_H__

#define CHN_1                (1<<3) // <! 1000mA
#define CHN_2                (1<<4)
#define CHN_3                (1<<2) // <! alternate function SERVO_PWM
#define CHN_4                (1<<1)
#define CHN_5                (1<<0) // <! 1000mA

#define LED_1                CHN_2 //<! Rumpf Unterseite (rot)
#define LED_2                CHN_4 //<! Rumpf Oberseite (weiß)
#define LED_3                CHN_1 //<! Navi Leds (grün/rot)
#define LED_4                CHN_5 //<! Lande Scheinwerfer

#define SERVO_PWM            CHN_3 //<! PWM Signal Lande Gestell Servo

#define SERVO_THRESHOLD      24

#define CHECK_SERVO_THRESHOLD(v) (v >= SERVO_THRESHOLD)

#endif /* BOARDS_V_2_H__ */