#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "config.h"

#ifdef USE_STEPS
#define STEPS USE_STEPS
#else
#define STEPS (sizeof(led_state_table))
#endif

#define MIN(a, b) ((a < b) ? a : b)

void init_led(void);
void init_servo_peripherals(void);
int main(void);

typedef enum {
    SERVO_STATE_INIT,
    SERVO_STATE_WAIT,
    SERVO_STATE_MEASURE,
} servo_state_t;

struct {
    uint8_t overflow_ctr;
    servo_state_t state;
    uint8_t current_level;
    uint8_t timestamp;
} static volatile servo_measure_ctx;

const uint8_t led_state_table[] = LED_STATE_TABLE;

int main(void) {
    // Init all peripherals
    // --------------------
    init_led();
    init_servo_peripherals();

    // Init led state counter and LED output var
    // and warning timeout and the in use outmsk
    // -----------------------------------------
    uint8_t state_ctr = 0;
    uint8_t warning_ctr = 0;
    uint8_t active_out_msk = WARNING_LED;

    // Init servo reading context
    // --------------------------
    servo_measure_ctx.state = SERVO_STATE_INIT;
    uint8_t servo_level_ctr = 0;
    _Bool servo_old = 0;

    _delay_ms(1000); //<! Short init phase to test leds

    PORTB &= ~LED_3; //<! Turn off LED_3 again 

    sei(); //<! Turn on interrupts    

    // Loop trough the LED blink state machine
    // forever
    // ---------------------------------------
    while(1) {
        _delay_ms(STEP_PERIOD); //<! Sleep for the step period

        // Set the LED out I/O var to the correct state
        // for each led, by requesting state from state
        // tables.
        // --------------------------------------------
        PORTB = (led_state_table[MIN(state_ctr, sizeof(led_state_table) - 1)] & active_out_msk) | (PORTB & (LED_4 | LED_3)); //<! Write current LED status to I/O register & msk with LED mask
        
        // Toggle LED_3 only when not warn
        // -------------------------------
        if (active_out_msk != WARNING_LED && (state_ctr % 2) == 1)
            PORTB |= LED_3;
        else
            PORTB &= ~LED_3;

        state_ctr++; //<! Increment the state
        if (warning_ctr < START_WARNING_TIME)
            warning_ctr++;
        else if (warning_ctr == START_WARNING_TIME) {
            active_out_msk |= OUT_MSK;
            state_ctr = 0;
            warning_ctr++;
        }

        // If last step was finished just now, then
        // reset to first state.
        // ----------------------------------------
        if (state_ctr >= STEPS)
            state_ctr = 0;

        // Check if the servo level changed in comparison to the old  level
        // ----------------------------------------------------------------
        _Bool current_level = CHECK_SERVO_THRESHOLD(servo_measure_ctx.current_level);
        if (current_level && current_level != servo_old) {
            // It changed, so update reference and reset counter
            // -------------------------------------------------
            servo_old = current_level;
            servo_level_ctr = 0;
        } else if (!current_level) {
            // In case of falling edge, just turn off the LED
            // ----------------------------------------------
            PORTB &= ~LED_4;
            servo_old = current_level;
        } else {
            // Did not change, update and check counter
            // ----------------------------------------
            if (servo_level_ctr == SERVO_TIMEOUT) {
                PORTB |= LED_4;
                servo_level_ctr++;
            } else if (servo_level_ctr < SERVO_TIMEOUT) {
                servo_level_ctr++;
            }
        }
    }

    return 0;
}

/**
 * Timer overflowed, so increase
 */
ISR(TIMER0_OVF_vect) {
    // Just increment the overflow counter by one
    // ------------------------------------------
    servo_measure_ctx.overflow_ctr++;
}

/**
 * Pin change interrupt.
 * Either start the timer, or read the time.
 */
ISR(PCINT0_vect) {
    switch (servo_measure_ctx.state) {
        case SERVO_STATE_INIT:
            // When we go from high to low, then init is done.
            // First low phase
            // -----------------------------------------------
            if (!(PINB & SERVO_PWM))
                servo_measure_ctx.state = SERVO_STATE_WAIT;
            break;
        case SERVO_STATE_WAIT:
            if (PINB & SERVO_PWM) {
                servo_measure_ctx.overflow_ctr = 0; //<! Reset OVF.
                servo_measure_ctx.timestamp = TCNT0; //<! Read current timer value
                servo_measure_ctx.state = SERVO_STATE_MEASURE;
            }
            break;
        case SERVO_STATE_MEASURE:
            if (!(PINB & SERVO_PWM)) {
                // Calculate current servo value
                // -----------------------------
                if (servo_measure_ctx.overflow_ctr == 0)
                    servo_measure_ctx.current_level = TCNT0 - servo_measure_ctx.timestamp;
                else
                    servo_measure_ctx.current_level = (0xff - servo_measure_ctx.timestamp) + TCNT0;
                
                servo_measure_ctx.state = SERVO_STATE_WAIT;
            }
            break;
    }
}

/**
 * Init timer and PCIE on the servo GPIO.
 */
void init_servo_peripherals(void) {
    // Setting up the pin change interrupt.
    // ------------------------------------
    GIMSK |= (1<<PCIE); //<! Enable pin change interrupt
    PCMSK |= SERVO_PWM; //<! Unmask the SERVO:PWM pin, to get interrupts

    // Set up the timer to a reasonable resolution.
    // --------------------------------------------
    TCCR0B |= ((1<<CS01) | (1<<CS00)); //<! Select clock source clkIO with 64 prescaler
    TIMSK |= (1<<TOIE0); //<! Enable overflow interrupt, because well it might overflow
}

/**
 * Init all LEDS GPIOS to output and also
 * set them to high for initialization.
 * This should give us a "ON" effect.
 */
void init_led(void) {
    DDRB |= (OUT_MSK | LED_4 | LED_3);
    PORTB |= (OUT_MSK | LED_4 | LED_3);
}