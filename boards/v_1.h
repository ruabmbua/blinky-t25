#ifndef BOARDS_V_1_H__
#define BOARDS_V_1_H__

#define CHN_1                (1<<4) //<! Rumpf Unterseite
#define CHN_2                (1<<3) //<! Rumpf Oberseite
#define CHN_3                (1<<2) //<! Flügel
#define CHN_4                (1<<1) //<! Lande Scheinwerfer
#define CHN_5                (1<<0) //<! PWM Signal Servo

#endif /* BOARDS_V_1_H__ */