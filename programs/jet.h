#ifndef PROGRAMS_JET_H__
#define PROGRAMS_JET_H__

#define SERVO_TIMEOUT        (2000 / STEP_PERIOD)

#define STEP_PERIOD          150

#define WARNING_LED          0
#define START_WARNING_TIME   0

#define OUT_MSK              (LED_1 | LED_2 | LED_3)

#define LED_STATE_TABLE {\
        LED_1 | LED_3,\
        0,\
        LED_1 | LED_3,\
        0,\
        LED_3,\
        LED_2,\
        LED_2 | LED_3,\
        LED_2,\
        LED_2 | LED_3,\
        0\
    }\

// -----------------------------------------------------------------------

#endif /* PROGRAMS_JET_H__ */