#ifndef BOARDS_V_2_1_H__
#define BOARDS_V_2_1_H__

#define CHN_1                (1<<2) // <! Blitzlicht
#define CHN_2                (1<<1) // <! PWM dimmed LED (nav & pos LED)
#define CHN_3                (1<<0) // <! Beacon (ALC)
#define CHN_4                (1<<3) // <! Landescheinwerfer
#define CHN_5                (1<<4) // <! alternate function SERVO_PWM


#define LED_1                CHN_3 //<! Rumpf Unterseite (rot)
#define LED_2                CHN_1 //<! Rumpf Oberseite (weiß)
#define LED_3                CHN_2 //<! Navi Leds (grün/rot)
#define LED_4                CHN_4 //<! Lande Scheinwerfer

#define SERVO_PWM            CHN_5 //<! PWM Signal Lande Gestell Servo

#define SERVO_THRESHOLD      24

#define CHECK_SERVO_THRESHOLD(v) (v < SERVO_THRESHOLD)

#endif /* BOARDS_V_2_1_H__ */