# Blinky T25

Ein Led Controller gebaut mit einem *ATtiny25-20PU*.

## Funktionen

* Seperate Ansteuerung von 4 LED`s mit jeweils 5V 300mA (1,5W)
* Lesen von einem digitalen / analogen RC Servo Signal

**Peripherie:**

* 4x LED Steckplätze
* 1x RC Empfänger Kanal / Stromversorgung Steckplatz
* 1x RC Servo Steckplatz (gesteuert von RC Empfänger Kanal Steckplatz)

## Schaltplan

![](res/schaltplan.png)

## Software

Zurzeit kann das Controller Programm nur 2 LED`s (LED1 und 2) blinken lassen.
Geplant ist ein voll reprogrammierbarer universell einsetzbarer Controller,
welcher durch Anwendersoftware reprogrammierbar ist.