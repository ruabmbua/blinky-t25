#ifndef PROGRAMS_FMS_AVANTI_S_H__
#define PROGRAMS_FMS_AVANTI_S_H__

#define SERVO_TIMEOUT        (2000 / STEP_PERIOD)
#define START_WARNING_TIME   (5000 / STEP_PERIOD)
#define WARNING_LED          LED_1

#define STEP_PERIOD          80

#define OUT_MSK              (LED_1 | LED_2 | LED_3)

#define LED_STATE_TABLE {\
        LED_1,\
        LED_3,\
        LED_1,\
        LED_3,\
        LED_2,\
        LED_3,\
        LED_2,\
        LED_3,\
        LED_2,\
        LED_3,\
        0\
    }\

#define USE_STEPS 15

// #undef CHECK_SERVO_THRESHOLD
// #define CHECK_SERVO_THRESHOLD(v) (v >= SERVO_THRESHOLD) //<! This is a override
// -----------------------------------------------------------------------

#endif /* PROGRAMS_FMS_AVANTI_S_H__ */