#ifndef PROGRAMS_LEGACY_H__
#define PROGRAMS_LEGACY_H__

#define LED_1                CHN_1 //<! Rumpf Unterseite
#define LED_2                CHN_2 //<! Rumpf Oberseite
#define LED_3                CHN_3 //<! Flügel
#define LED_4                CHN_4 //<! Lande Scheinwerfer

#define SERVO_PWM            CHN_5 //<! PWM Signal Lande Gestell Servo0

#define SERVO_THRESHOLD      24

#define SERVO_TIMEOUT        (2000 / STEP_PERIOD)

#define STEP_PERIOD          150

#define OUT_MSK              (LED_1 | LED_2 | LED_3)

#define LED_STATE_TABLE {\
        LED_1,\
        0,\
        LED_1,\
        LED_3,\
        0,\
        0,\
        LED_2 | LED_3,\
        0,\
        LED_2,\
        0\
    }\

// -----------------------------------------------------------------------

#endif /* PROGRAMS_LEGACY_H__ */