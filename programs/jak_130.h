#ifndef PROGRAMS_JAK_130_H__
#define PROGRAMS_JAK_130_H__

#define SERVO_TIMEOUT        (2000 / STEP_PERIOD)
#define START_WARNING_TIME   (5000 / STEP_PERIOD)
#define WARNING_LED          LED_1

#define STEP_PERIOD          80

#define OUT_MSK              (LED_1 | LED_2)

#define LED_STATE_TABLE {\
        LED_1,\
        0,\
        LED_1,\
        0,\
        LED_2,\
        0,\
        LED_2,\
        0,\
        LED_2,\
        0\
    }\

#define USE_STEPS 13

#undef CHECK_SERVO_THRESHOLD
#define CHECK_SERVO_THRESHOLD(v) (v >= SERVO_THRESHOLD) //<! This is a override
// -----------------------------------------------------------------------

#endif /* PROGRAMS_JAK_130_H__ */